#pragma once

#include <random>
#include <string>

#include "base_entity.hpp"
#include "difficulty.hpp"
#include "random_number_generator.hpp"

/**
 * gets a random slot at first which decides if up- or sideways and places them
 * in this direction
 */
class EasyAI : public BaseEntity {
public:
  EasyAI(const std::string &name, Color color, Difficulty difficulty)
      : BaseEntity(name, color, difficulty), chosen_slot_(Column(0)),
        last_ai_col_(Column(0)), first_run_(true), is_vertical_(false),
        rand_num_() {}

  Column choose_slot(const Board &board) override;

private:
  Column chosen_slot_;
  Column last_ai_col_;
  bool first_run_;
  bool is_vertical_;
  RandomNumberGenerator rand_num_;
};
