#pragma once

#include <iostream>

struct Column {
  using size_type = std::size_t;

  explicit Column(size_type i) : value(i) {}

  size_type value;
};
