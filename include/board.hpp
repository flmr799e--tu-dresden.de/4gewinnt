#pragma once

#include <algorithm>
#include <iostream>

#include "color.hpp"
#include "matrix.hpp"

class Board {
public:
  using size_type = Matrix<Color>::size_type;

  Board(Row rows, Column columns, Column last_coordinate)
      : matrix_(Coordinate(rows, columns), Color::empty),
        last_col_(last_coordinate) {}

  void show_board() const;
  bool drop_coin(Color color, Column slot);

  Color win() const;
  bool draw() const;

  size_type rows() const;
  size_type columns() const;

  bool is_full(Column column) const;

  /**
   * last played coordinate on the board (not player related)
   */
  Column last_col() const;
  void add_last_col(Column coord);

private:
  Matrix<Color> matrix_;
  Column last_col_;
};
