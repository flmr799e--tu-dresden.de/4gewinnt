#include "easy_ai.hpp"

#include <iostream>

Column EasyAI::choose_slot(const Board &board) {
  if (first_run_) {
    chosen_slot_ = Column(rand_num_.generate(0, board.columns() - 1));
    first_run_ = false;
  }
  auto slot = chosen_slot_;
  last_ai_col_ = slot;
  if (is_vertical_) {
    if (last_ai_col_.value == board.columns() - 1) {
      slot = Column(0);
    } else {
      slot.value++;
    }
    while (board.is_full(slot)) {
      slot.value++;
      if (slot.value++ > board.columns()){
        slot = Column(0);
      }
    }

  } else {
    while (slot.value < board.columns() && board.is_full(slot)) {
      slot.value++;
    }
  }
  is_vertical_ = !is_vertical_;
  chosen_slot_ = slot;
  last_ai_col_ = slot;
  return slot;
}
