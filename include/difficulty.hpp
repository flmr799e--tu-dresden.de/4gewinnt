#pragma once

#include <iostream>
enum class Difficulty { player, baby, easy, medium, hard };
std::ostream &operator<<(std::ostream &os, const Difficulty &e);
