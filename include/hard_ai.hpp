#pragma once

#include <random>
#include <string>

#include "base_entity.hpp"
#include "difficulty.hpp"
#include "random_number_generator.hpp"

/**
 * has a simple logic but is smarter than easy and normal
 */
class HardAI : public BaseEntity {
public:
  HardAI(const std::string &name, Color color, Difficulty difficulty)
      : BaseEntity(name, color, difficulty), last_ai_col_(Column(0)),
        choosen_slot_(Column(0)), first_run_(true), rand_num_() {}

  Column choose_slot(const Board &board) override;
  Column when_full(Column slot, const Board &board);

private:
  std::string side_;
  Column last_ai_col_;
  Column choosen_slot_;
  bool first_run_;
  RandomNumberGenerator rand_num_;
};
