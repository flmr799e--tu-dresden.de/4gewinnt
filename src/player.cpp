#include "player.hpp"

/**
 * lets the user choose a slot and returns the column
 */
Column Player::choose_slot(const Board &board) {
  while (true) {
    std::string slot_string;
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    while (std::cin.get() != '\n') {
      continue;
    }
    std::cout << name() << " - choose a slot [1-" << board.columns() << "]: ";
    std::getline(std::cin, slot_string);
    std::cout << std::endl;

    long long unsigned slot;
    bool invalid_str = false;
    try {
      slot = std::stoull(slot_string);
    } catch (std::invalid_argument &e) {
      std::cout << "please use a valid number" << std::endl;
      slot_string.clear();
      invalid_str = true;
    }
    if (!invalid_str) {
      if (Column column(slot - 1);
          slot <= board.columns() && slot > 0 && !board.is_full(column)) {
        return column;
      } else {
        std::cout << "please use a valid number" << std::endl;
      }
    }
  }
}
