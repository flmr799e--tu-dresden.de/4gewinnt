#include "game.hpp"

#include "easy_ai.hpp"
#include "hard_ai.hpp"
#include "medium_ai.hpp"
#include "player.hpp"

#include <iostream>
#include <unistd.h>

/**
 * starts the game and runs until there is a draw or win
 */
void Game::start() {
  if (player1_.difficulty() == Difficulty::player) {
    std::cout << std::endl;
    std::cout << "board:" << std::endl;
    board_.show_board();
    std::cout << std::endl;
  }

  bool running = true;
  while (running) {
    if (play_game(0)) {
      break;
    }
    if (play_game(1)) {
      break;
    }
  }
}

bool Game::play_game(int player_num) {
  auto &player = (player_num == 0) ? player1_ : player2_;
  auto &opponent = (player_num == 0) ? player2_ : player1_;

  Column slot = player.choose_slot(board_);
  while (!board_.drop_coin(player.color(), slot)) {
    slot = player.choose_slot(board_);
  }

  std::cout << player.name() << " - " << player.difficulty() << std::endl;
  board_.show_board();
  std::cout << std::endl;

  if (board_.win() == Color::red || board_.win() == Color::yellow) {
    std::cout << player.name() << " [" << board_.win() << "] has won!"
              << std::endl;
    board_.add_last_col(slot);
    return true;
  } else if (board_.draw()) {
    std::cout << "It's a draw!" << std::endl;
    board_.add_last_col(slot);
    return true;
  } else if (opponent.difficulty() != Difficulty::player) {
    sleep(1);
  }
  board_.add_last_col(slot);
  return false;
}
