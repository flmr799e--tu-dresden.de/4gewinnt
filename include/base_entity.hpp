#pragma once

#include <iostream>
#include <string>

#include "board.hpp"
#include "column.hpp"
#include "difficulty.hpp"

class BaseEntity {
public:
  BaseEntity(const std::string &name, Color color, Difficulty difficulty)
      : name_(name), color_(color), difficulty_(difficulty) {}

  std::string name() const;
  Color color() const;

  virtual Column choose_slot(const Board &board) = 0;

  virtual ~BaseEntity() = default;

  Difficulty difficulty() const;

private:
  std::string name_;
  Color color_;
  Difficulty difficulty_;
};
