#pragma once

#include <random>

class RandomNumberGenerator {
public:
  RandomNumberGenerator() : engine_(std::random_device{}()) {}

  int generate(int min, int max) {
    std::uniform_int_distribution<int> distribution(min, max);
    return distribution(engine_);
  }

private:
  std::mt19937 engine_;
};
