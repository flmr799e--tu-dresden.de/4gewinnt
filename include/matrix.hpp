#pragma once

#include <cassert>
#include <iostream>
#include <vector>

#include "coordinate.hpp"

template <typename T> class Matrix {
public:
  using size_type = std::size_t;
  using value_type = T;

  Matrix() = delete;

  explicit Matrix(Coordinate size, const value_type &val = value_type())
      : vector_(size.column.value * size.row.value, val), size_(size) {
    assert(!vector_.empty());
  }

  Matrix(const Matrix &) = default;
  Matrix(Matrix &&) = default;
  Matrix &operator=(const Matrix &) = default;
  Matrix &operator=(Matrix &&) = default;

  int cell(Column column, Row row) const {
    assert((row.value <= size_.row.value) && "Message: Row is out of range!");
    assert((column.value <= size_.column.value &&
            "Message: Column is out of range!"));
    assert(row.value >= 0);
    assert(column.value >= 0);
    auto temp = column.value + size_.column.value * row.value;
    // assert(temp < vector_.size() && temp >= 0);
    return temp;
  }

  value_type &operator()(Column column, Row row) {
    return vector_.at(cell(column, row));
  }

  const value_type &operator()(Column column, Row row) const {
    return vector_.at(cell(column, row));
  }

  size_type rows() const { return size_.row.value; }

  size_type columns() const { return size_.column.value; }

private:
  std::vector<T> vector_;
  Coordinate size_;
};
