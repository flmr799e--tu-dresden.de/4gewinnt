#include "base_entity.hpp"

std::string BaseEntity::name() const { return name_; }
Color BaseEntity::color() const { return color_; }
Difficulty BaseEntity::difficulty() const { return difficulty_; }
