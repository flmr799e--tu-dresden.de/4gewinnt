#pragma once

#include <iostream>

struct Row {
  using size_type = std::size_t;

  explicit Row(size_type i) : value(i) {}

  size_type value;
};
