#pragma once

#include "base_entity.hpp"
#include "board.hpp"
#include "hard_ai.hpp"
#include "medium_ai.hpp"
#include "player.hpp"

class Game {
public:
  Game(BaseEntity &player1, BaseEntity &player2)
      : board_(Row(6), Column(7), Column(0)), player1_(player1),
        player2_(player2) {}

  void start();
  bool play_game(int player_num);

private:
  Board board_;
  BaseEntity &player1_;
  BaseEntity &player2_;
};
