# 4Gewinnt

## How to add a new difficulty

- add the difficulty name to the difficulty enum and to it's cpp file in src
- make a new header file inside of the lib folder with the name of the new difficulty which should be based of the BaseEntity class
- write your program and write the functions in the src folder

---
added later:

- add the possible difficulty to src/main to make_ai() and to choose_difficulty() so that the user can choose the new difficulty
