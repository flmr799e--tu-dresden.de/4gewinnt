#include "medium_ai.hpp"
#include "random_number_generator.hpp"
#include <iostream>

Column MediumAI::choose_slot(const Board &board) {
  int rand_slot;
  do {
    RandomNumberGenerator rand_num;
    rand_slot = rand_num.generate(0, board.columns() - 1);
  } while (board.is_full(Column(rand_slot)));
  return Column(rand_slot);
}
