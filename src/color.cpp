#include "color.hpp"

#include <cassert>
#include <iostream>

std::ostream &operator<<(std::ostream &os, const Color &e) {
  switch (e) {
  case Color::red:
    return os << "💢";
    break;
  case Color::yellow:
    return os << "⭐";
    break;
  case Color::empty:
    return os << "➖";
    break;
  }
  return os;
}

Color operator~(Color e) {
  assert(e != Color::empty);
  return e == Color::red ? Color::yellow : Color::red;
}
