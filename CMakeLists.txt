cmake_minimum_required(VERSION 3.11.0)
project(4Gewinnt VERSION 0.2.1)

include(CTest)
enable_testing()

add_library(ai_lib SHARED
    src/mirror_ai.cpp
)

add_executable(VierGewinnt
    src/main.cpp
    src/base_entity.cpp
    src/board.cpp
    src/color.cpp
    src/difficulty.cpp
    src/easy_ai.cpp
    src/game.cpp
    src/hard_ai.cpp
    src/medium_ai.cpp
    src/player.cpp
)

target_include_directories(VierGewinnt PRIVATE
    include
    lib
)

target_compile_features(VierGewinnt PUBLIC cxx_std_20)
target_compile_options(VierGewinnt PUBLIC -Wall -pedantic -Wextra)

target_link_libraries(VierGewinnt PRIVATE ai_lib)
