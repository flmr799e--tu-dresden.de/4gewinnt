#pragma once

#include <random>
#include <string>

#include "base_entity.hpp"
#include "board.hpp"

class Player : public BaseEntity {
public:
  Player(const std::string &name, Color color, Difficulty difficulty)
      : BaseEntity(name, color, difficulty) {}

  Column choose_slot(const Board &board) override;

private:
};
