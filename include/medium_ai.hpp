#pragma once

#include <random>
#include <string>

#include "base_entity.hpp"
#include "difficulty.hpp"

/**
 * chooses a random slot in range of the board and returns the column
 */
class MediumAI : public BaseEntity {
public:
  MediumAI(const std::string &name, Color color, Difficulty difficulty)
      : BaseEntity(name, color, difficulty) {}

  Column choose_slot(const Board &board) override;

private:
};
