#include "board.hpp"

#include <algorithm>
#include <iostream>

using size_type = Matrix<Color>::size_type;

/**
 * prints out the current board
 */
void Board::show_board() const {
  for (size_type i = 0; i < rows(); i++) {
    for (size_type j = 0; j < columns(); j++) {
      std::cout << "[" << matrix_(Column(j), Row(i)) << "]";
    }
    std::cout << std::endl;
  }
}

/**
 * set's the value on the right place in the matrix
 */
bool Board::drop_coin(Color color, Column slot) {
  if (slot.value >= columns()) {
    return false;
  }
  if (is_full(slot)) {
    return false;
  }
  for (int i = rows() - 1; i >= 0; i--) {
    if (matrix_(slot, Row(i)) == Color::empty) {
      matrix_(slot, Row(i)) = color;
      add_last_col(slot);
      return true;
    }
  }
  return false;
}

/**
 * check for a win and return true if a game is won
 */
Color Board::win() const {
  for (auto i = 0ull; i < rows(); i++) {
    for (auto j = 0ull; j < columns(); j++) {
      auto color = matrix_(Column(j), Row(i));
      if (color == Color::empty)
        continue;

      // check for horizontal win
      if (j <= columns() - 4 && color == matrix_(Column(j + 1), Row(i)) &&
          color == matrix_(Column(j + 2), Row(i)) &&
          color == matrix_(Column(j + 3), Row(i))) {
        return color;
      }

      // check for vertical win
      if (i <= rows() - 4 && color == matrix_(Column(j), Row(i + 1)) &&
          color == matrix_(Column(j), Row(i + 2)) &&
          color == matrix_(Column(j), Row(i + 3))) {
        return color;
      }

      // check for diagonal win (top-left to bottom-right)
      if (j <= columns() - 4 && i <= rows() - 4 &&
          color == matrix_(Column(j + 1), Row(i + 1)) &&
          color == matrix_(Column(j + 2), Row(i + 2)) &&
          color == matrix_(Column(j + 3), Row(i + 3))) {
        return color;
      }

      // check for diagonal win (bottom-left to top-right)
      if (j <= columns() - 4 && i >= 3 &&
          color == matrix_(Column(j + 1), Row(i - 1)) &&
          color == matrix_(Column(j + 2), Row(i - 2)) &&
          color == matrix_(Column(j + 3), Row(i - 3))) {
        return color;
      }
    }
  }

  return Color::empty;
}

/**
 * check for a draw and return true if the game is a draw
 */
bool Board::draw() const {
  for (auto i = 0ull; i < matrix_.columns(); i++) {
    for (auto j = 0ull; j < matrix_.rows(); j++) {
      if (matrix_(Column(i), Row(j)) == Color::empty) {
        return false;
      }
    }
  }
  return true;
}

size_type Board::rows() const { return matrix_.rows(); }
size_type Board::columns() const { return matrix_.columns(); }

bool Board::is_full(Column column) const {
  if (matrix_(column, Row(0)) != Color::empty) {
    return true;
  }
  return false;
}

Column Board::last_col() const { return last_col_; }
void Board::add_last_col(Column coord) { last_col_ = coord; }
