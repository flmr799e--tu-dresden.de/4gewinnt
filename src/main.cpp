#include <iostream>
#include <memory>
#include <string>
#include <tuple>
#include <unordered_set>

#include "board.hpp"
#include "difficulty.hpp"
#include "easy_ai.hpp"
#include "game.hpp"
#include "hard_ai.hpp"
#include "medium_ai.hpp"
#include "mirror_ai.hpp"
#include "player.hpp"

std::string name(const std::string &player) {
  std::string player_name;
  while (player_name == "") {
    std::cout << "Enter the " << player << " name: ";
    std::getline(std::cin, player_name);
  }
  return player_name;
}

std::tuple<Color, Color> colors() {
  std::string input;
  while (input != "yellow" && input != "red") {
    std::cout << "Choose a color for first player [Red/yellow]: ";
    std::getline(std::cin, input);
    if (input.empty()) {
      input = "red";
    }
  }

  auto color = input == "red" ? Color::red : Color::yellow;

  return std::make_tuple(color, ~color);
}

Difficulty choose_difficulty(const std::string &name) {
  std::string difficulty_str;
  while (difficulty_str != "baby" && difficulty_str != "easy" &&
         difficulty_str != "medium" && difficulty_str != "hard") {
    std::cout << "which difficulty would you like for " << name
              << " [baby/easy/Medium/hard]: ";
    std::getline(std::cin, difficulty_str);
    if (difficulty_str.empty()) {
      difficulty_str = "medium";
    }
  }
  if (difficulty_str == "baby") {
    return Difficulty::baby;
  } else if (difficulty_str == "easy") {
    return Difficulty::easy;
  } else if (difficulty_str == "medium") {
    return Difficulty::medium;
  }
  return Difficulty::hard;
}

std::unique_ptr<BaseEntity> make_ai(const std::string &name, Color color) {
  Difficulty difficulty = choose_difficulty(name);

  switch (difficulty) {
  case Difficulty::baby:
    return std::make_unique<MirrorAI>(name, color, Difficulty::baby);
  case Difficulty::easy:
    return std::make_unique<EasyAI>(name, color, Difficulty::easy);
  case Difficulty::medium:
    return std::make_unique<MediumAI>(name, color, Difficulty::medium);
  case Difficulty::hard:
    return std::make_unique<HardAI>(name, color, Difficulty::hard);
  default:
    throw std::runtime_error("Invalid difficulty selected");
  }
}

bool start_menu() {
  // start or exit game
  std::unordered_set<std::string> yes{"y", "yes", "Yes"};
  std::unordered_set<std::string> no{"n", "no"};
  std::string start_game;
  bool end_game = false;

  while (!end_game) {
    while (yes.count(start_game) == 0 && no.count(start_game) == 0) {
      std::cout << "Start game [Y/n]: ";
      std::getline(std::cin, start_game);
      if (start_game.empty()) {
        start_game = "yes";
      }
    }

    if (no.count(start_game)) {
      std::string response;

      while (yes.count(response) == 0 && no.count(response) == 0) {
        std::cout << "Are you sure you want to exit [y/N]: ";
        std::getline(std::cin, response);
        if (response.empty()) {
          response = "no";
        }
      }

      if (yes.count(response)) {
        std::cout << "Thanks for playing!" << std::endl;
        return false;
      } else {
        start_game = "";
      }
    } else {
      end_game = true;
    }
  }
  return true;
}

void run_game() {
  // the main game loop
  bool playing = true;
  while (playing) {
    if (!start_menu()) {
      break;
    }
    std::unique_ptr<BaseEntity> player1;
    std::unique_ptr<BaseEntity> player2;

    // Choose game mode
    std::string game_mode;
    while (game_mode != "pvp" && game_mode != "pvc" && game_mode != "cvc") {
      std::cout << "What gamemode do you want [pvp/Pvc/cvc]: ";
      std::getline(std::cin, game_mode);
      if (game_mode.empty()) {
        game_mode = "pvc";
      }
    }

    if (game_mode == "pvp") {
      // choose a color
      auto [color1, color2] = colors();

      player1 =
          std::make_unique<Player>(name("player1"), color1, Difficulty::player);
      player2 =
          std::make_unique<Player>(name("player2"), color2, Difficulty::player);
    } else if (game_mode == "pvc") {
      // choose a color
      auto [color1, color2] = colors();

      player1 =
          std::make_unique<Player>(name("player"), color1, Difficulty::player);
      player2 = make_ai("CPU", color2);

    } else {
      player1 = make_ai("CPU 1", Color::red);
      player2 = make_ai("CPU 2", Color::yellow);
    }

    Game game = Game(*player1, *player2);
    game.start();

    std::getline(std::cin, game_mode);
  }
}

int main() {
  run_game();
  return 0;
}
